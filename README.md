# Expensify

A simple react Boilerplate for simple React Apps.

## Getting Started

1. Clone the repo.
2. cd into the directory and run:
```
    npm install
```
3. run:
```
    npm run dev-server
```

to start a local instance.

### Prerequisites

A web browser, npm package manager.

## Built With

* [React](https://reactjs.org/) - A JavaScript library for building user interfaces.
* [Babel](https://babeljs.io/) - A JavaScript compiler.
* [Webpack](https://webpack.js.org/) - An Asset Bundler.

## Authors

* **Matthew Cahn** - *Initial work* - [mpcahn.co](https://mpcahn.co)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* **Andrew Mead** - *Creator:* [Udemy: The Complete React Web Developer Course (with Redux)](https://www.udemy.com/react-2nd-edition) - Created following this guide.

