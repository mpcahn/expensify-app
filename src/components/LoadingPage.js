import React from 'react'

const LoadingPage = () => (
  <div className="loader">
    <img
      className="loader__image"
      src="/img/loading.gif"
      alt="Loading..." />
  </div>
)

export default LoadingPage