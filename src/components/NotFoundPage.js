import React from 'react'
import { Link } from 'react-router-dom'

const NotFoundPage = () => (
  <h1>
    404 <Link to='/'>Go Home</Link><br/>
  </h1>
)

export default NotFoundPage