import * as firebase from 'firebase'

const config = {
  apiKey: process.env.FIREBASE_API_KEY,
  authDomain: process.env.FIREBASE_AUTH_DOMAIN,
  databaseURL: process.env.FIREBASE_DATABASE_URL,
  projectId: process.env.FIREBASE_PROJECT_ID,
  storageBucket: process.env.FIREBASE_STORAGE_BUCKET,
  messagingSenderId: process.env.FIREBASE_MESSAGING_SENDER_ID
}

firebase.initializeApp(config)

const database = firebase.database()
const googleAuthProvider = new firebase.auth.GoogleAuthProvider()

export { firebase, googleAuthProvider, database as default }

// // child_removed
// database.ref('expenses').on('child_removed', (snapshot) => {
//   console.log(snapshot.key, snapshot.val())
// })

// // child_changed
// database.ref('expenses').on('child_changed', (snapshot) => {
//   console.log(snapshot.key, snapshot.val())
// })

// // child_added -called for existing and new children
// database.ref('expenses').on('child_added', (snapshot) => { 
//   console.log(snapshot.key, snapshot.val())
// })

// // subscribe to array based data
// const onValueChange = database.ref('expenses')
//   .on('value', (snapshot) => {
//     const expensesArr = []
//     snapshot.forEach((childSnapshot) => {
//       expensesArr.push({
//         id: childSnapshot.key,
//         ...childSnapshot.val()
//       })
//     })
//     console.log(expensesArr)
//   })


// expenses.map(expense => {
//   delete expense.id
//   database.ref('expenses').push(expense)
// })


// database.ref('notes/-L_sZM2JI0RQ5L3iU0I3').remove()

// database.ref('notes').push(
//   {
//     title: 'Course Topics',
//     body: 'React, python, etc'
//   }
// )

// const firebaseNotes = {
//   notes: {
//     eaffafes11: {
//       title: 'first note',
//       body:
//     }
//   }
// }

// const notes = [{
//   id: '12',
//   title: 'first note',
//   body: 'This is a note'
// }, {
//   id: '13',
//   title: 'second note',
//   body: 'This is another note'
// }]

// database.ref('notes').set(notes)


// const onValueChange = database.ref().on('value', (snapshot) => {
//   let obj = snapshot.val()
//   let name = obj.name
//   let { company, title } = obj.job
//   console.log(`${name} is a ${title} at ${company}`)
// }, (e) => {
//   console.log('data fetch error: ', e)
// })

// database.ref().update({
//   name: 'Matthew Cahn',
//   'job/company': 'Zepplin',
//   'job/title': 'Blockchain Developer'
// })

// setTimeout(() => {
//   database.ref().update({
//     name: 'Emily Alice',
//     'job/company': 'Elizabeth Warren',
//     'job/title': 'GIS Analyst'
//   })
// }, 4269)


// const onValueChange = database.ref().on('value', (snapshot) => {
//   console.log(snapshot.val()) // subscribe to changes
// }, (e) => {
//   console.log('data fetch error: ', e)
// })

// setTimeout(() => {
//   database.ref().update({
//     age: 29,
//     'job/company': 'Consensys'
//   })
// }, 3200)

// setTimeout(() =>{
//   database.ref().off(value, onValueChange) // cancel subscription
// }, 6200)

// setTimeout(() =>{
//   database.ref().update({
//     age: 42,
//     'job/company': 'Consensys'
//   })
// }, 9200)

// //FETCH
// database.ref('location/city')
// .once('value')
// .then((snapshot)=> {
//   const val = snapshot.val()
//   console.log(val)
// })
// .catch((e)=>{
//   console.log('Error fetching data: ', e)
// })

// //SET DATA
// database.ref().set({
//   name: 'Matthew Cahn',
//   age: 32,
//   stressLevel: 6,
//   job: {
//     title: 'Blockchain Developer',
//     company: 'Zepplin'
//   },
//   location: {
//     Country: 'USA',
//     City: 'Boulder'
//   }
// }).then(() => {
//   console.log("Data Saved")
// }).catch((e) => {
//   console.log('error: ' +e)
// })

// // UPDATE 
// database.ref().update({
//   stressLevel: 9,
//   'job/company': 'Amazon', //update nested object  
//   'location/city': null // DELETE CITY
// })

//DELETING
// database.ref().remove()
//   .then(() => {
//     console.log("data deleted")
//   }).catch((e) => {
//     console.log("error: "+e)
//   })

// database.ref('isSingle').set(null) // Also deletes

//FETCHING