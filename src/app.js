import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import AppRouter, { history } from './routers/AppRouter.js'
import LoadingPage from './components/LoadingPage'

import configureStore from './store/configureStore'
import { firebase } from './firebase/firebase'
import { login, logout } from './actions/auth'
import { startSetExpenses } from './actions/expenses'
import 'normalize.css/normalize.css' //Import Normalize.css
import './styles/styles.scss'
import 'react-dates/lib/css/_datepicker.css'

const store = configureStore()

//Provider allows us to provide store to any component that needs it
const jsx = (
  <Provider store={ store }>
    <AppRouter />
  </Provider>  
)

let hasRendered = false
const renderApp = () => {
  if (!hasRendered){
    ReactDOM.render(jsx, document.getElementById('app'))
    hasRendered = true
  }
}

ReactDOM.render(<LoadingPage />, document.getElementById('app'))

firebase.auth().onAuthStateChanged((user) => {
  if (user) {
    store.dispatch(login(user.uid))    
    store.dispatch(startSetExpenses()).then(() => {
      renderApp()
      if (history.location.pathname === '/') {
        history.push('/dashboard')
      }
    })
  } else {
    store.dispatch(logout())    
    renderApp()
    history.push('/')
  }
})
