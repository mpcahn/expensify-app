import React from 'react'
import ReactDOM from 'react-dom'

const Info = (props) => (
  <div>
    <h1>info</h1>
    <p>Hi there, the info is: {props.info} <br/> {props.greeting}</p>
  </div>
)

const withAdminWarning = (WrappedComponent) => {
  return (props) => (
    <div>
      {props.isAdmin && <p>This is private info. Please don't share</p>}
      <WrappedComponent {...props} />
    </div>
  )
}

// require auth

const requireAuthentication = (WrappedComponent) => {
  return (props) => (
    <div>
      {props.isAuthenticated ? (
        <div>
          <p>You are authenticated!</p>
          <WrappedComponent {...props} />
        </div>
      ):(
          <div>You are not authenticated!</div>
      )}
    </div>
  )
}

const AuthInfo = requireAuthentication(Info)
const AdminInfo = withAdminWarning(Info)

// ReactDOM.render(<AdminInfo isAdmin={true} info="WASSSUUUUPPPP!!!!" greeting="dis MATTMAN!!" />, document.getElementById('app'))
ReactDOM.render(<AuthInfo isAuthenticated={true} info="WASSSUUUUPPPP!!!!" greeting="dis MATTMAN!!" />, document.getElementById('app'))