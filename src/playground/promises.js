const promise = new Promise((resolve, reject) => {
  setTimeout(() => { reject('something went wrong') }, 3000)  
})

console.log("before")

promise.then((data) => {
  console.log(data)
}).catch((error) => {
  console.log('error: '+error)
})

//then can be used with catch

// promise.then((data) => {
//   console.log(data)
// }, (error) => {
//   console.log('error: '+error)
// })

console.log("after")