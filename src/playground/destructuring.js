// const person = {
//   name: 'Matt',
//   age: 32,
//   location: {
//     city: 'Denver',
//     temp: 36
//   }
// }

// const { name: firstName = 'Anonymous', age } = person //default value if no name
// console.log(`${firstName} is ${age}`)

// const { city, temp: temperature } = person.location //renames temp temperature
// if ( city && temperature){
//   console.log(`It's ${temperature} in ${city}`)
// }

// const book = {
//   title: 'The Wings to Awakening',
//   author: 'Thanissaro Bhikkhu',
//   publisher: {
//     // name: 'Dhamma Dana'
//   }
// }

// const { name: publisherName = 'Self-Published' } = book.publisher
// console.log(publisherName) 

const address = ['3925 17th st','Boulder', 'CO', '80303']
const [ , city = 'Springfield', state = 'USA' ] = address
console.log(`You are in ${city}, ${state}`)

const item = ['Coffee (h)', '$.99', '$1.50', '$2.00']
const [ title, , mediumPrice ] = item
console.log (`a ${title} costs ${mediumPrice}`)