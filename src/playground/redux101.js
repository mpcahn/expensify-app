import { createStore } from 'redux'

// Action generators - functions that return action objects.
const incrementCount = ({ incrementBy = 1 } = {}) => ({
  type: 'INCREMENT_COUNT',
  incrementBy
})

const decrementCount = ({ decrementBy = 1 } = {}) => ({
  type: 'DECREMENT_COUNT',
  decrementBy
})

const resetCount = () => ({
  type: 'RESET_COUNT'
})

const setCount = ({ count } = {}) => ({
  type: 'SET',
  count
})

// REDUCERS
// 1. Reducers are pure functions.
// 2. Never change state or action.
const countReducer = (state = { count: 0 }, action) => {
  switch (action.type) {
    // Increment
    case 'INCREMENT_COUNT':
      return {
        count: state.count + action.incrementBy
      }
    case 'DECREMENT_COUNT':      
      return {
        count: state.count - action.decrementBy
      }
    case 'SET':
      return {
        count: action.count
      }
    case 'RESET_COUNT':
      return {
        count: 0
      }
    default:
      return state
  }
}

// CREATE REDUX STORE
const store = createStore(countReducer)

// Log state everytime it changes until unsubscribe is called
const unsubscribe = store.subscribe(() => {
  console.log(store.getState())
})

// DISPATCH ACTION GENERATORS TO STORE
store.dispatch(incrementCount())
store.dispatch(incrementCount({ incrementBy: 3 }))
store.dispatch(incrementCount())
store.dispatch(incrementCount())
store.dispatch(decrementCount({ decrementBy: 10 }))
store.dispatch(decrementCount())
store.dispatch(setCount({ count: 40 }))
store.dispatch(resetCount())
unsubscribe()
store.dispatch(incrementCount())
