import React from 'react'
import { connect } from 'react-redux'
import { Route, Redirect } from 'react-router-dom'

export const PublicRoute = ({
  AuthBool,
  component: Component,
  ...rest // puts everything we didn't destructure into variable
}) => (
    <Route {...rest} component={(props) => (
      AuthBool ? (
        <Redirect to="/dashboard" />
      ) : (
          <Component {...props} />
        )
    )} />
  )

const mapStateToProps = (state) => ({
  AuthBool: !!state.auth.uid
})

export default connect(mapStateToProps)(PublicRoute)