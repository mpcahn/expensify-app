import { Router, Route, Switch } from 'react-router-dom'
import React from 'react'
import createHistory from 'history/createBrowserHistory'
import AddExpensePage from '../components/AddExpensePage'
import ExpenseDashboardPage from '../components/ExpenseDashboardPage'
import EditExpensePage from '../components/EditExpensePage'
import LoginPage from '../components/LoginPage'
import NotFoundPage from '../components/NotFoundPage'
import PrivateRoute from './PrivateRoute'
import PublicRoute from './PublicRoute'

export const history = createHistory()

const AppRouter = () => (
  <Router history={history}>
    <div>
      <Switch>
        <PublicRoute exact={ true } path='/' component={ LoginPage } />
        <PrivateRoute path='/dashboard' component={ ExpenseDashboardPage } />
        <PrivateRoute path='/create' component={ AddExpensePage } />
        {/* Gives access to id value */}
        <PrivateRoute path='/edit/:id'  component={ EditExpensePage } /> 
        <Route component={ NotFoundPage } />
      </Switch>
    </div>
  </Router>
)

export default AppRouter
