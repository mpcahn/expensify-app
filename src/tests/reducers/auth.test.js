import authReducer from '../../reducers/auth'

test('should set default state', () => {
  const state = authReducer(undefined, { type: '@@INIT' })
  expect(state).toEqual({})
})

test('should set uid when logged in', () => {
  const action = {
    type: 'LOGIN',
    uid: 'abcd1234'
  }

  const state = authReducer({}, action)
  expect(state.uid).toBe(action.uid)
})

test('should clear uid when logged out', () => {
  const state = authReducer({ uid: 'abcd1234' }, { type: 'LOGOUT' })
  expect(state).toEqual({})
})

