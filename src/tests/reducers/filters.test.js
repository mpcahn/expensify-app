import filtersReducer from '../../reducers/filters'
import moment from 'moment'

test('setup default filter values', () => {
  const state = filtersReducer(undefined, { type: '@@INIT' })
  expect(state).toEqual({
    text: '',
    sortBy: 'date',
    startDate: moment().startOf('month'),
    endDate: moment().endOf('month')
  })
})

test('set sortBy to amount', () => {
  const state = filtersReducer(undefined, { type: 'SORT_BY_AMOUNT' })
  expect(state.sortBy).toBe('amount')
})

test('set sortBy to date', () => {
  const currentState = {
    text: '',
    startDate: undefined,
    endDate: undefined,
    sortBy: 'amount'
  }
  const action = { type: 'SORT_BY_DATE' }
  const state = filtersReducer(currentState, action)
  expect(state.sortBy).toBe('date')
})

test('set text filter', () => {
  const state = filtersReducer(undefined, {type: 'SET_TEXT_FILTER', text: 'bill'})
  expect(state.text).toBe('bill')
})

test('set startDate filter', () => {
  const startDate = moment()
  const state = filtersReducer(undefined, { type: 'SET_START_DATE', startDate })
  expect(state.startDate).toEqual(startDate)
})

test('set endDate filter', () => {
  const endDate = moment()
  const state = filtersReducer(undefined, { type: 'SET_END_DATE', endDate })
  expect(state.endDate).toEqual(endDate)
})
