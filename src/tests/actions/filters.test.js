import { 
  setStartDate, 
  setEndDate, 
  sortByAmount, 
  sortByDate, 
  setTextFilter 
} from '../../actions/filters'
import moment from 'moment'

test('generate setStartDate action object', ()=>{
  const action = setStartDate(moment(0))
  expect(action).toEqual({
    type: 'SET_START_DATE',
    startDate: moment(0)
  })
})

test('generate setEndDate action object', ()=>{
  const action = setEndDate(moment(0))
  expect(action).toEqual({
    type: 'SET_END_DATE',
    endDate: moment(0)
  })
})

test('generate sortByAmount action object', ()=>{
  const action = sortByAmount()
  expect(action).toEqual({
    type: 'SORT_BY_AMOUNT'
  })
})

test('generate sortByDate action object', ()=>{
  const action = sortByDate()
  expect(action).toEqual({
    type: 'SORT_BY_DATE'
  })
})

test('generate setTextFilter action object with given values', ()=>{
  const action = setTextFilter('bill')
  expect(action).toEqual({
    type: 'SET_TEXT_FILTER',
    text: 'bill'
  })
})

test('generate setTextFilter action object with default values', ()=>{
  const action = setTextFilter()
  expect(action).toEqual({
    type: 'SET_TEXT_FILTER',
    text: ''
  })
})
