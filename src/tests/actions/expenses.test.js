import configureMockStore from 'redux-mock-store'
import database from '../../firebase/firebase'

import thunk from 'redux-thunk'
import { 
  startAddExpense, 
  addExpense, 
  startEditExpense,
  editExpense,
  startRemoveExpense,  
  removeExpense,   
  setExpenses, 
  startSetExpenses
  } from '../../actions/expenses'
import expenses from '../fixtures/expenses'

const uid = '123asd'
const defaultAuthState = { auth: { uid } }
const createMockStore = configureMockStore([thunk])

beforeEach((done) => {
  const expensesData = {}
  expenses.forEach(({ id, description, note, amount, createdAt }) => {
    expensesData[id] = { description, note, amount, createdAt }
  })
  database.ref(`users/${uid}/expenses`).set(expensesData).then(() => done())
})

test('should remove expense from firebase', (done) => {
  const store = createMockStore(defaultAuthState)
  const id = expenses[1].id
  store.dispatch(startRemoveExpense({ id }))
    .then(() => {
      const actions = store.getActions()
      expect(actions[0]).toEqual({
        type: 'REMOVE_EXPENSE',
        id
      })
      return database.ref(`users/${uid}/expenses/${id}`).once('value')
    }).then((snapshot) => {
      expect(snapshot.val()).toBeFalsy()
      done()
    })
})

test('setup removeExpense action object', () => {
  const action = removeExpense({ id: '123abc' })
  expect(action).toEqual({ // Use with objects or arrays
    type: 'REMOVE_EXPENSE',
    id: '123abc'
  })
})

test('should edit expense from firebase', (done) => {
  const store = createMockStore(defaultAuthState)
  const id = expenses[1].id
  const updates = { amount: 160000 }

  store.dispatch(startEditExpense(id, updates))
    .then(() => {
      const actions = store.getActions()
      expect(actions[0]).toEqual({
        type: 'EDIT_EXPENSE',
        id,
        updates
      })
      return database.ref(`users/${uid}/expenses/${id}`).once('value')
    }).then((snapshot) => {
      expect(snapshot.val().amount).toBe(updates.amount)
      done()
    })
})

test('setup editExpense action object', () => {
  const action = editExpense('abc123', { note: 'testing123' })
  expect(action).toEqual({
    type: 'EDIT_EXPENSE',
    id: 'abc123',
    updates: { note: 'testing123' }
  })
})

test('setup add expense action object with provided values', () => {
  const action = addExpense(expenses[0])
  expect(action).toEqual({
    type: 'ADD_EXPENSE',
    expense: expenses[0]
  })
})

// use done for async tests
test('Should add expense to firebase and store', (done) => {
  const store = createMockStore(defaultAuthState)
  const expenseData = {
    description: 'Asus Zephyrus',
    amount: 250000,
    note: 'Modern...',
    createdAt: 1000
  }

  store.dispatch(startAddExpense(expenseData)).then(() => {
    const actions = store.getActions()
    expect(actions[0]).toEqual({
      type: 'ADD_EXPENSE',
      expense: {
        id: expect.any(String),
        ...expenseData
      }
    })

    database.ref(`users/${uid}/expenses/${actions[0].expense.id}`).once('value')
      .then((snapshot) => {
        expect(snapshot.val()).toEqual(expenseData)

        // TELLS JEST ASYNC CALLS ARE DONEF
        done()
      })
  })
})

test('Should add expense with defaults to Firebase and store', (done) => {
  const store = createMockStore(defaultAuthState)
  const expenseDefault = {
    description: '',
    note: '',
    amount: 0,
    createdAt: 0
  }

  store.dispatch(startAddExpense({})).then(() => {
    const actions = store.getActions()
    expect(actions[0]).toEqual({
      type: 'ADD_EXPENSE',
      expense: {
        id: expect.any(String),
        ...expenseDefault
      }
    })

    database.ref(`users/${uid}/expenses/${actions[0].expense.id}`).once('value')
      .then((snapshot) => {
        expect(snapshot.val()).toEqual(expenseDefault)
        done()
      })
  })
})

test('should set expense action object with data', () => {
  const action = setExpenses(expenses)
  expect(action).toEqual({
    type: 'SET_EXPENSES',
    expenses
  })
})

test('should fetch expenses from firebase', (done) => {
  const store = createMockStore(defaultAuthState)
  store.dispatch(startSetExpenses()).then(() => {
    const actions = store.getActions();
    expect(actions[0]).toEqual({
      type: 'SET_EXPENSES',
      expenses
    })
    done()
  })
})