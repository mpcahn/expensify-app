import React from 'react'
import { shallow } from 'enzyme'
import toJSON from 'enzyme-to-json'
import ExpenseListItem from '../../components/ExpenseListItem'
import expenses from '../fixtures/expenses'

test('should rendre expenseListItem correctly', () => {
  const expense = expenses[0]
  const wrapper = shallow(<ExpenseListItem key={expense.id} {...expense} />)
  expect(toJSON(wrapper)).toMatchSnapshot()
})

