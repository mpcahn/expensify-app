import React from 'react'
import { shallow } from 'enzyme'
import toJSON from 'enzyme-to-json'
import expenses from '../fixtures/expenses'
import ExpenseForm from '../../components/ExpenseForm'
import moment from 'moment'

test('should render ExpenseForm correctly', () => {
  const wrapper = shallow(<ExpenseForm />)
  expect(toJSON(wrapper)).toMatchSnapshot()
})

test('should render ExpenseForm correctly with expense data', () => {
  const expense = expenses[0]
  const wrapper = shallow(<ExpenseForm expense={ expense } />)
  expect(toJSON(wrapper)).toMatchSnapshot()
})

test('should render error for invalid form submission', () => {
  const wrapper = shallow(<ExpenseForm />)
  expect(toJSON(wrapper)).toMatchSnapshot()
  wrapper.find('form').simulate('submit', {
    preventDefault: () => {}
  })
  expect(wrapper.state('error').length).toBeGreaterThan(0)
  expect(toJSON(wrapper)).toMatchSnapshot()
})

test('should set description on input change', () => {
  const value = 'Hello there...'
  const wrapper = shallow(<ExpenseForm />)
  wrapper.find('input').at(0).simulate('change', {
    target: { value }
  })
  expect(wrapper.state('description')).toBe(value)
})

test('should set note on textarea change', () => {
  const value = "General Kenobi"
  const wrapper = shallow(<ExpenseForm />)
  wrapper.find('textarea').simulate('change', {
    target: { value }
  })
  expect(wrapper.state('note')).toBe(value)
})

test('should set amount on valid input (about tree fiddy)', () => {
  const value = "3.50"
  const wrapper = shallow(<ExpenseForm />)
  wrapper.find('input').at(1).simulate('change', {
    target: { value }
  })
  expect(wrapper.state('amount')).toBe(value)
})

test('should not set amount on invalid input', () => {
  const value = "23.555"
  const wrapper = shallow(<ExpenseForm />)
  wrapper.find('input').at(1).simulate('change', {
    target: { value }
  })
  expect(wrapper.state('amount')).toBe('')
})

//Spies
test('should call onSubmit prop for valid form submission', () => {
  const onSubmitSpy = jest.fn()
  const expenseSubmission = {...expenses[0]}
  delete expenseSubmission.id

  const wrapper = shallow(<ExpenseForm expense={expenses[0]} onSubmit={onSubmitSpy} />)
  wrapper.find('form').simulate('submit', {
    preventDefault: () => {}
  })
  expect(wrapper.state('error')).toBe('')
  expect(onSubmitSpy).toHaveBeenLastCalledWith(expenseSubmission)
})

test('should set new date on date change', () => {
  const now = moment()
  const wrapper = shallow(<ExpenseForm />)
  wrapper.find('SingleDatePicker').prop('onDateChange')(now)
  expect(wrapper.state('createdAt')).toEqual(now)
})

test('should set calendar focus on change', () => {
  const focused = true
  const wrapper = shallow(<ExpenseForm />)
  wrapper.find('SingleDatePicker').prop('onFocusChange')({ focused })
  expect(wrapper.state('calFocused')).toBe(focused)
})