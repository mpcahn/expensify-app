const expensesReducerDefaultState = []

export default (
  state = expensesReducerDefaultState,
  action
) => {
  switch (action.type) {
    case 'ADD_EXPENSE':
      console.log("ADD EXPENSE")
      return [...state, action.expense]
    case 'REMOVE_EXPENSE':
      console.log("REMOVE EXPENSE")
      return state.filter(expense => expense.id !== action.id)
    case 'EDIT_EXPENSE':
      console.log("EDIT EXPENSE")
      return state.map((expense) => {
        if (expense.id === action.id) {
          return {
            ...expense,
            ...action.updates
          }
        } else {
          return expense
        }
      })
    case 'SET_EXPENSES':
    console.log("SET EXPENSE")
      return action.expenses
    default:
      return state
  }
}

